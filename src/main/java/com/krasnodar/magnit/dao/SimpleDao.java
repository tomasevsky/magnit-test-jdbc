package com.krasnodar.magnit.dao;

import com.krasnodar.magnit.entity.EntryEntity;

import java.util.List;

/**
 * Author: Oleg Tomashevsky
 * Date: 18.03.2015
 * Time: 14:24
 */
public interface SimpleDao {
    void deleteAllRows();

    List<EntryEntity> getAllRows();

    int[] batchInsert(final List<EntryEntity> list);
}
