package com.krasnodar.magnit.dao;

import com.krasnodar.magnit.entity.EntryEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Author: Oleg Tomashevsky
 * Date: 18.03.2015
 * Time: 13:53
 */
public class SimpleDaoImpl implements SimpleDao {
    private static final String INSERT_ROW = "insert into TEST(FIELD) values (?)";
    private static final String SELECT_ALL_ROWS = "select FIELD from TEST";
    private static final String DELETE_ALL_ROWS = "delete from TEST";

    private JdbcTemplate jdbcTemplate;

    public SimpleDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int[] batchInsert(final List<EntryEntity> list) {
        return jdbcTemplate.batchUpdate(
                INSERT_ROW,
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setInt(1, (list.get(i)).getField());
                    }

                    public int getBatchSize() {
                        return list.size();
                    }
                });
    }

    @Override
    public void deleteAllRows() {
        jdbcTemplate.update(DELETE_ALL_ROWS, new Object[]{}, new int[]{});
    }

    @Override
    public List<EntryEntity> getAllRows() {
        return jdbcTemplate.query(
                SELECT_ALL_ROWS, new Object[]{}, new EntryMapper());

    }

    private static final class EntryMapper implements RowMapper<EntryEntity> {

        public EntryEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
            EntryEntity entryEntity = new EntryEntity();
            entryEntity.setField(Integer.valueOf(rs.getString("FIELD")));
            return entryEntity;
        }
    }
}
