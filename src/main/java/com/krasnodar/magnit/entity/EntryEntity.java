package com.krasnodar.magnit.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Author: Oleg Tomashevsky
 * Date: 18.03.2015
 * Time: 16:54
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EntryEntity {
    @XmlElement
    private Integer field;

    public Integer getField() {
        return field;
    }

    public void setField(Integer field) {
        this.field = field;
    }
}
