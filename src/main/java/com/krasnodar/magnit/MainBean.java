package com.krasnodar.magnit;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import com.krasnodar.magnit.dao.SimpleDao;
import com.krasnodar.magnit.entity.EntryEntity;
import com.krasnodar.magnit.entity.EntryListEntity;
import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Oleg Tomashevsky
 * Date: 18.03.2015
 * Time: 12:37
 */
public class MainBean {
    private SimpleDao simpleDao;
    private Integer n;

    public MainBean() {
    }

    public long executeAll() throws JAXBException, TransformerException, IOException, SAXException {
        generateAndSaveData();

        File file1 = new File("1.xml");
        generateXmlFromData(file1);

        File file2 = new File("2.xml");
        transformWithXslt(file1, file2);

        return parseAndFindSum(file2);
    }

    public void generateAndSaveData() {
        simpleDao.deleteAllRows();
        ArrayList<EntryEntity> list = new ArrayList<EntryEntity>(n);
        for (int i = 1; i <= n; i++) {
            EntryEntity entryEntity = new EntryEntity();
            entryEntity.setField(i);
            list.add(entryEntity);
        }
        simpleDao.batchInsert(list);
    }

    public void generateXmlFromData(File file) throws JAXBException {
        List<EntryEntity> list = simpleDao.getAllRows();
        EntryListEntity entryListEntity = new EntryListEntity();
        entryListEntity.setEntries(list);

        JAXBContext jaxbContext = JAXBContext.newInstance(EntryListEntity.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(entryListEntity, file);
    }

    public void transformWithXslt(File sourceFile, File destFile) throws TransformerException, IOException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(new FileReader(this.getClass().getClassLoader().getResource("transform.xslt").getFile()));
        Transformer transformer = factory.newTransformer(xslt);

        Source source = new StreamSource(sourceFile);
        transformer.transform(source, new StreamResult(destFile));
    }

    public long parseAndFindSum(File file) throws IOException, SAXException {
        long sum = 0;
        DOMParser parser = new DOMParser();

        FileInputStream inputStream = new FileInputStream(file);
        parser.parse(new InputSource(inputStream));

        Document doc = parser.getDocument();
        doc.normalizeDocument();
        doc.getDocumentElement().normalize();
        Element root = doc.getDocumentElement();
        NodeList elements = root.getElementsByTagName("entry");
        for (int i = 0; i < elements.getLength(); i++) {
            Node entryNode = elements.item(i);
            Node fieldAttr = entryNode.getAttributes().getNamedItem("field");
            String fieldValue = fieldAttr.getNodeValue();
            sum += Integer.valueOf(fieldValue);
        }
        return sum;
    }

    private InputStream getResourceAsStream(String file) throws IOException {
        return new ClassPathResource(file, getClass()).getInputStream();
    }

    public void setSimpleDao(SimpleDao simpleDao) {
        this.simpleDao = simpleDao;
    }

    public void setN(Integer n) {
        this.n = n;
    }
}
